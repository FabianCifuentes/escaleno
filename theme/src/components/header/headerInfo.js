import React from 'react';

export default class HeaderInfo extends React.Component {
	render() {
		return (
			<div className="header-info">
				<div className="container">
					<div className="columns is-gapless">
						<div className="column is-8 has-text-left">
							<a href="http://www.marsol.cl" className="has-text-light">
								www.MARSOL.cl
							</a>
						</div>
						<div className="column is-2 has-text-right">
							<a href="http://www.marsol.cl" className="has-text-light">
								Compras anteriores
							</a>
						</div>
						<div className="column is-2 has-text-right">
							<a href="http://www.marsol.cl" className="has-text-light">
								Servicio técnico:
							</a>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
