import React from 'react';

export default class HeaderLogin extends React.PureComponent {
	render() {
		return (
			<div className="columns">
				<div className="column">
					<i class="fas fa-user" />
				</div>
				<div className="column has-text-left">
					<h6 className="has-text-white">Iniciar Sesion</h6>
					<a href="#" className="has-text-warning">
						Mi Cuenta
					</a>
				</div>
			</div>
		);
	}
}
